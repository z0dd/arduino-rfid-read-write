#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         9
#define SS_PIN          10

MFRC522 mfrc522(SS_PIN, RST_PIN);
MFRC522::MIFARE_Key key;

byte trailerBlock   = 1;
byte sector         = 0;
byte blockAddr      = 1;
byte status;
byte buffer[18];
byte size = sizeof(buffer);
byte writeBuffer[18];

/*
ERROR CODES:{
  0 : 'This only works with MIFARE Classic cards',
  1 : 'Authentificate failed',
  2 : 'Read card fail',
  3 : 'Write card fail'
}
*/

void setup() {
  Serial.begin(9600);
  while (!Serial);
  SPI.begin();
  mfrc522.PCD_Init();

  // используем ключ FFFFFFFFFFFFh который является стандартом для пустых карт
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
}

void loop() {
  if ( ! mfrc522.PICC_IsNewCardPresent()) return;               // Ждем новую карту
  if ( ! mfrc522.PICC_ReadCardSerial()) return;                 // Выбираем одну из карт
  if (checkComapre())
  {
    if (Serial.available()) {              //если есть доступные данные
      byte len;
      len = Serial.readBytesUntil('#', (char *) writeBuffer, 30); // read family name from serial

      for (byte i = len; i < 30; i++) writeBuffer[i] = '\s';

      makeAuth();
      writeId();
    }
    makeAuth();
    readId();
  }

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
}

void dump_byte_array(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : "");
    if (buffer[i] != 115) {
      Serial.print((char)buffer[i]);
    }
  }
}

void readId() {
    // mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);   // Дампаем сектор памяти
    status = mfrc522.MIFARE_Read(blockAddr, buffer, &size);                       // Читаем данные из блока
    if (status != MFRC522::STATUS_OK) {
      Serial.println(F("ERROR : 2"));
      // Serial.println(mfrc522.GetStatusCodeName(status));
      return;
    } else {
      Serial.print(F("User ID:"));
      dump_byte_array(buffer, 16);
      Serial.println();
    }
}

void writeId() {
  status = mfrc522.MIFARE_Write(trailerBlock, writeBuffer, 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.println(F("ERROR : 3"));
    // Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  } else {
    Serial.println(F("Write success"));
  }
}

void makeAuth() {
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.println(F("ERROR : 1"));
    // Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
}

bool checkComapre() {
  byte piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  if (    piccType != MFRC522::PICC_TYPE_MIFARE_MINI
          &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
          &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("ERROR : 0"));
    return false;
  } else {
    return true;
  }
}