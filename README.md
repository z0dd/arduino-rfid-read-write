# README #

Siple example for connect Arduino Uno and RFID rc522.
Read and write some data to card.

### Requirements ###

* library MFRC522.h (http://playground.arduino.cc/Learning/MFRC522)
* Connection:
![RFID _522png.png](https://bitbucket.org/repo/xBjLj5/images/3295994930-RFID%20_522png.png)

### How to use: ###

* Collect scheme;
* install MFRC522.h;
* Upload sketch to your Arduino Uno
* Open COM port to Arduino
* Attach you RDIF card to RC522
* You will see current data writed ad block
* Send {some_test}# to serial
* Attach you RDIF card to RC522
* PROFIT!